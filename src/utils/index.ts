import { isEmpty } from "lodash";

export function stringIncludesOrEmpty(haystack: any, niddle: string) {
  const data = (haystack = String(haystack));
  return isEmpty(niddle) || data.includes(niddle);
}
