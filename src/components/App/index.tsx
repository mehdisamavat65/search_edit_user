import React from "react";
import { UserManager } from "../UserManager";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { Login } from "../Login";
import "./style.css";

function App() {
  return (
    <div className="App">
      <Router>
        <Route exact path="/" component={Login} />
        <Route path="/usermanager" component={UserManager} />
      </Router>
    </div>
  );
}

export default App;
