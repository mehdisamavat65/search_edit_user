import React from "react";
import { Col, Container, Row } from "react-bootstrap";
import { Storage } from "../../utils/storage";
import "./style.css";
import userListFromRepository from "./users.json";
import { UserTable } from "./UserTable";

interface Props {
  className?: string;
}

export const UserManager = ({ className }: Props) => {
  return (
    <Container fluid className={`UserManager ${className || ""}`}>
    

      <Row className="UsersTableWrapper">
        <Col>
          <UserTable data={userListFromRepository} />
        </Col>
      </Row>
    </Container>
  );
};
