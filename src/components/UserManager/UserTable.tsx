import React, { useCallback, useMemo, useState } from "react";
import { Button, Form, Table, Row, Col } from "react-bootstrap";
import { PAGINATOR_SIZE } from "../../constants";
import { stringIncludesOrEmpty } from "../../utils";
import { EditModal } from "../EditModal";
import { Paginator } from "../Paginator";
import { GROUP_TYPES, POSITION_TYPES, UNIT_TYPES } from "./constants";
import "./style.css";
import { User } from "./types";
import { RecordWithTtl } from "dns";
import { Storage } from "../../utils/storage";

export interface UserTableProps {
  data: User[];
}

export const UserTable = ({ data }: UserTableProps) => {
  const [page, setPage] = useState(0);
  const [showFilterData, setShowFilterData] = useState(false);
  const [itemforEdit, setItemforEdit] = useState<User | undefined>();
  const [editUser, setEditUser] = useState<User | undefined>();
  const [search, setSearch] = useState({
    idCard: "",
    idPerson: "",
    name: "",
    position: POSITION_TYPES,
    group: GROUP_TYPES,
    unit: UNIT_TYPES,
  });
  let mainData = useMemo<User[]>(() => {
    return data.slice(
      page * PAGINATOR_SIZE,
      page * PAGINATOR_SIZE + PAGINATOR_SIZE
    );
  }, [data, page]);

  const filteredData = useMemo<User[]>(() => {
    return data
      .filter((item) => {
        return (
          stringIncludesOrEmpty(item.idCard, search.idCard) &&
          stringIncludesOrEmpty(item.idPerson, search.idPerson) &&
          stringIncludesOrEmpty(item.name, search.name) &&
          search.position.includes(item.position) &&
          search.unit.includes(item.unit) &&
          search.group.includes(item.group)
        );
      })
      .map((user) =>
        user.idCard === editUser?.idCard
          ? {
              ...user,
              name: editUser.name,
              position: editUser.position,
              unit: editUser.unit,
              group: editUser.group,
            }
          : user
      )
      .slice(page * PAGINATOR_SIZE, page * PAGINATOR_SIZE + PAGINATOR_SIZE);
  }, [
    data,
    editUser,
    page,
    search.group,
    search.idCard,
    search.idPerson,
    search.name,
    search.position,
    search.unit,
  ]);
  const handlePageChange = useCallback((page) => setPage(page), []);
  const handleInputSearchChange = useCallback(
    (e) => {
      setSearch({
        ...search,
        [e.currentTarget.dataset.filterKey]: e.currentTarget.value,
      });
    },
    [search]
  );
  const handleEditUser = useCallback(
    (event: React.MouseEvent<HTMLSpanElement, MouseEvent>) => {
      const dataClick = event.currentTarget.dataset as any;

      if (dataClick) {
        const item = data.find((x) => String(x.idCard) === dataClick.id);
        if (item) {
          setItemforEdit(item);
        }
      }
    },
    [data]
  );
  const handleChangeEditUser = useCallback((item: User) => {
    setItemforEdit(undefined);
    setEditUser(item);
  }, []);
  const handleSearch = useCallback(() => {
    setShowFilterData(true);
  }, []);

  return (
    <div className="UserTable">
      <Row>
        <Col>
          <h5>{Storage.read("username")} عزیز خوش آمدید</h5>
        </Col>
        <Col>
          <h5 className="countTitle"> تعداد: {filteredData.length}</h5>
        </Col>
      </Row>

      <Table striped bordered hover>
        <thead>
          <tr>
            <th style={{ textAlign: "center" }}></th>
            <th>
              <Form.Control
                type="text"
                data-filter-key="idPerson"
                onChange={handleInputSearchChange}
              />
            </th>
            <th>
              <Form.Control
                type="text"
                data-filter-key="name"
                onChange={handleInputSearchChange}
              />
            </th>
            <th className="text-center" colSpan={4}>
              <Button block variant="primary" onClick={handleSearch}>
                جستجو
              </Button>
            </th>
          </tr>
          <tr>
            <th>شماره کارت</th>
            <th>شماره پرسنلی</th>
            <th>نام</th>
            <th>سمت</th>
            <th>واحد</th>
            <th>گروه</th>
            {showFilterData && <th>عملیات</th>}
          </tr>
        </thead>
        <tbody>
          {showFilterData
            ? filteredData.map((user: User) => (
                <tr key={user.idCard}>
                  <td>{user.idCard}</td>
                  <td>{user.idPerson}</td>
                  <td>{user.name}</td>
                  <td>{user.position}</td>
                  <td>{user.unit}</td>
                  <td>{user.group}</td>
                  <td
                    className="edit"
                    data-id={user.idCard}
                    onClick={handleEditUser}
                  >
                    {" "}
                    ویرایش
                  </td>
                </tr>
              ))
            : mainData.map((user: User) => (
                <tr key={user.idCard}>
                  <td>{user.idCard}</td>
                  <td>{user.idPerson}</td>
                  <td>{user.name}</td>
                  <td>{user.position}</td>
                  <td>{user.unit}</td>
                  <td>{user.group}</td>
                </tr>
              ))}
        </tbody>
      </Table>
      <div
        style={{
          display: "flex",
          flexDirection: "row-reverse",
          justifyContent: "space-between",
        }}
      >
        <Paginator
          totalPage={data.length / PAGINATOR_SIZE}
          currentPage={page}
          onChange={handlePageChange}
        />
      </div>
      {itemforEdit && (
        <EditModal item={itemforEdit} onChange={handleChangeEditUser} />
      )}
    </div>
  );
};
