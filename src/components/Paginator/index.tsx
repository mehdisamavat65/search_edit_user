import { range } from "lodash";
import React, { useCallback } from "react";
import { Pagination } from "react-bootstrap";
import { PAGINATOR_SIZE } from "../../constants";
import "./style.css";

export interface PaginatorProps {
  totalPage: number;
  currentPage: number;
  onChange: (page: number) => void;
}

export const Paginator = ({
  currentPage,
  totalPage,
  onChange,
}: PaginatorProps) => {

  const handleChange = useCallback(
    (e) => {
      onChange(e.currentTarget.dataset.page);
    },
    [onChange]
  );

  return (
    <Pagination className="Pagination"
     previous-text="World" next-text="Good" 
 last-text="Day!">
      {+currentPage > 0 && (
        <Pagination.Prev
          key={"prev"}
          data-page={currentPage - 1}
          onClick={handleChange}
        />
      )}

      {range(0, totalPage).map((page) => (
        <Pagination.Item
          key={page}
          activeLabel={""}
          active={+page === +currentPage}
          data-page={page}
          onClick={handleChange}
        >
          {page + 1}
        </Pagination.Item>
      ))}

      {Math.ceil(totalPage / PAGINATOR_SIZE) > currentPage && (
        <Pagination.Next
          key={"next"}
          data-page={currentPage + 1}
          onClick={handleChange}
        />
      )}
    </Pagination>
  );
};
