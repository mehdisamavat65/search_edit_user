import React, { useCallback, useState } from "react";
import { Button, Form, Modal } from "react-bootstrap";
import { User } from "../UserManager/types";
import "./style.css";

interface Props {
  item: User;

  onChange: (item: User) => void;
}

export const EditModal = ({ item, onChange }: Props) => {
  const [show, setShow] = useState(true);
  const [changeUserInfo, setChangeUserInfo] = useState({
    name: "",
    position: "",
    group: "",
    unit: "",
  });

  const handleClose = useCallback(() => setShow(false), []);
  const handleChange = useCallback(
    (e) => {
      onChange({
        ...item,
        name: changeUserInfo.name !== "" ? changeUserInfo.name : item.name,
        position:
          changeUserInfo.position !== ""
            ? changeUserInfo.position
            : item.position,
        unit: changeUserInfo.unit !== "" ? changeUserInfo.unit : item.unit,
        group: changeUserInfo.group !== "" ? changeUserInfo.group : item.group,
      });
    },
    [
      changeUserInfo.group,
      changeUserInfo.name,
      changeUserInfo.position,
      changeUserInfo.unit,
      item,
      onChange,
    ]
  );
  const handleChangeUserInfo = useCallback(
    (e) => {
      setChangeUserInfo({
        ...changeUserInfo,
        [e.target.name]: e.target.value,
      });
    },
    [changeUserInfo]
  );
 
  return (
    <div className="FilterModal">
      <Modal show={show} onHide={handleClose}>
        <Modal.Header>
          <Modal.Title>ویرایش کاربر</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form.Group controlId="formBasicName">
            <Form.Label>نام کاربری</Form.Label>
            <Form.Control
              name="name"
              type="text"
              placeholder="نام کاربری"
              defaultValue={item.name}
              onChange={handleChangeUserInfo}
            />
          </Form.Group>
          <Form.Group controlId="formBasicPosition">
            <Form.Label>سمت</Form.Label>
            <Form.Control
              name="position"
              type="text"
              placeholder="سمت"
              defaultValue={item.position}
              onChange={handleChangeUserInfo}
            />
          </Form.Group>
          <Form.Group controlId="formBasicUnit">
            <Form.Label>واحد</Form.Label>
            <Form.Control
              name="unit"
              type="text"
              placeholder="واحد"
              defaultValue={item.unit}
              onChange={handleChangeUserInfo}
            />
          </Form.Group>
          <Form.Group controlId="formBasicGroup">
            <Form.Label>گروه</Form.Label>
            <Form.Control
              name="group"
              type="text"
              placeholder="گروه"
              defaultValue={item.group}
              onChange={handleChangeUserInfo}
            />
          </Form.Group>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="danger" onClick={handleClose}>
            انصراف
          </Button>
          <Button variant="success" onClick={handleChange}>
            ثبت
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
};
