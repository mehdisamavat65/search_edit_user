export interface User {
   idCard:number;
   idPerson:number;
   name:string;
   position:string;
   unit:string;
   group:string;
  }