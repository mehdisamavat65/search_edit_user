import React from "react";
import { useState } from "react";
import { useCallback } from "react";
import { Col, Container, Row, Form, Button, Card } from "react-bootstrap";
import { Link, useHistory } from "react-router-dom";
import { Storage } from "../../utils/storage";
import "./style.css";

interface Props {
  className?: string;
}

export const Login = ({ className }: Props) => {
  const history = useHistory();


  const [changeUsername, setChangeUsername] = useState("");
  const [changePassword, setChangePassword] = useState("");

  const handleChangeUsername = useCallback((e) => {
    setChangeUsername(e.target.value);
  }, []);
  const handleChangePassword = useCallback((e) => {
    setChangePassword(e.target.value);
  }, []);

  const handleLogin = useCallback(() => {
    Storage.put("username", changeUsername);
    history.push("/usermanager")
  }, [changeUsername, history]);

  return (
    <Container fluid className={`UserManager ${className || ""}`}>
      <Card className="card">
        <Form className="form">
          <Form.Group controlId="formBasicEmail">
            <Form.Label>نام کاربری</Form.Label>
            <Form.Control
              type="username"
              placeholder="لطفا نام کاربری را وارد نمایید"
              onChange={handleChangeUsername}
            />
          </Form.Group>

          <Form.Group controlId="formBasicPassword">
            <Form.Label>رمز عبور</Form.Label>
            <Form.Control
              type="password"
              placeholder="لطفا رمز عبور را وارد نمایید"
              onChange={handleChangePassword}
            />
          </Form.Group>

          <Form.Group controlId="formBasicLogin" style={{ marginTop: "1em" }}>
            <Link to='userManager'>
            <Button variant="primary" type="submit" onClick={handleLogin}>
              ورود
            </Button>
            </Link>
          </Form.Group>
        </Form>
      </Card>
    </Container>
  );
};
